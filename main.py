from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QToolBar, QAction, QLayout, QDesktopWidget
from PyQt5.QtGui import QIcon, QFont
from PyQt5.QtCore import Qt, QSize
import requests
import sys
import json
from mainwindow import Ui_MainWindow


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.initUI()


    def initUI(self):
        self.__set_window_size()
        self.__initToolbar()

    def __set_window_size(self):
        ag = QDesktopWidget().availableGeometry()
        self.setGeometry(ag)

    def __initToolbar(self):
        action_vk = QAction(QIcon("vk.png"), "Вконтакте", self)
        action_vk.triggered.connect(self.__vk_clicked)
        action_vk.setFont(QFont("Noto sans", 20))

        action_insta = QAction(QIcon("instagram.png"), "Instagram", self)
        action_insta.triggered.connect(self.__insta_clicked)
        action_insta.setFont(QFont("Noto sans", 20))

        action_fb = QAction(QIcon("facebook.png"), "Facebook", self)
        action_fb.triggered.connect(self.__insta_clicked)
        action_fb.setFont(QFont("Noto sans", 20))

        action_ok = QAction(QIcon("ok.png"), "Одноклассники", self)
        action_ok.triggered.connect(self.__insta_clicked)
        action_ok.setFont(QFont("Noto sans", 20))

        self.ui.toolBar.addAction(action_vk)
        self.ui.toolBar.addAction(action_insta)
        self.ui.toolBar.addAction(action_fb)
        self.ui.toolBar.addAction(action_ok)

        self.ui.toolBar.setToolButtonStyle(Qt.ToolButtonTextBesideIcon)
        self.ui.toolBar.setIconSize(QSize(64, 64))

        layout = self.ui.toolBar.layout()
        for i in range(layout.count()):
            layout.itemAt(i).setAlignment(Qt.AlignLeft)

    def __vk_clicked(self):
        print("VK")

    def __insta_clicked(self):
        print("INSTA")

    def __facebook_clicked(self):
        print("FACEBOOK")

    def __ok_clicked(self):
        print("OK")



app = QtWidgets.QApplication([])
application = MainWindow()
application.show()

sys.exit(app.exec())


# def test():
#     t = requests.get("http://192.168.43.115:3000/vk/posts")
#     data = json.loads(t.text)
#     print(data)
#
#
# test()
